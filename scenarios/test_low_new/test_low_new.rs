{
	"fly_count_range" : {
		"min" : 1,
		"step" : 2,
		"max" : 30,
		"values" : [
		]
	},
	"length_range" : {
		"min" : 5,
		"step" : 10,
		"max" : 75,
		"values" : [
		]
	},
	"thread_pool_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
		]
	},
	"task_pool_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
		]
	},
	"debug_level" : 1,
	"iter_count" : 1,
	"map_size" : 256,
	"multi_thread_test" : true,
	"single_thread_test" : true,
	"res_type" : 1
}