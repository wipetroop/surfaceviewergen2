{
	"fly_count_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			2,
			4,
			8,
			16			
		],
		"ununsed_values" : [
			32,
			64
		]
	},
	"length_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			10,
			50,
			100
		]
	},
	"thread_pool_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			1,
			4,
			8
		],
		"ununsed_values" : [
		]
	},
	"task_pool_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			2,
			4,
			8
		],
		"ununsed_values" : [
		]
	},
	"debug_level" : 1,
	"iter_count" : 1,
	"map_size" : 400,
	"multi_thread_test" : true,
	"single_thread_test" : true,
	"res_type" : 2
}