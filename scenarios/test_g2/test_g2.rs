{
	"fly_count_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			1,
			2,
			64,
			128,
			256
		],
		"values_not_used" : [
			4,
			8,
			16,
			32
		]
	},
	"length_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			10,
			50,
			100
		]
	},
	"thread_pool_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			4,
			8,
			16
		],
		"not_used_values" : [
			1
		]
	},
	"task_pool_range" : {
		"min" : 1,
		"step" : 1,
		"max" : 1,
		"values" : [
			4,
			8,
			16,
			24,
			32,
			40,
			48,
			56,
			64,
			72,
			80,
			88,
			96,
			104,
			112,
			120,
			128
		],
		"not_used_values" : [
			2,
			1
		]
	},
	"debug_level" : 1,
	"iter_count" : 1,
	"map_size" : 512,
	"multi_thread_test" : true,
	"single_thread_test" : true,
	"res_type" : 2
}