#pragma once

struct tag
{
   static constexpr char static_obj[] = "static_obj";
   static constexpr char dynamic_obj[] = "dynamic_obj";

   static constexpr char type[] = "type";
   static constexpr char prop_list[] = "prop_list";
   static constexpr char contour_list[] = "contour_list";

   static constexpr char contour[] = "contour";

   static constexpr char key[] = "key";
   static constexpr char val[] = "val";

   static constexpr char lat[] = "lat";
   static constexpr char lon[] = "lon";
};