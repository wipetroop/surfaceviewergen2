#pragma once

#include "common/communicator.h"
#include "SimulatorBase.h"
#include "common/file_storage.h"
#include "crossdllinterface/SimulationInterface.h"
#include <array>

using iSimulatorPtr = std::unique_ptr<ColregSimulation::SimulatorBase, std::function<void(ColregSimulation::SimulatorBase*)>>;

class SimulatorManager : public ColregSimulation::iSimulatorManager, public Central
{
public:
   SimulatorManager(central_pack* pack) : Central(pack) {}

   void Release() override final { delete this; }

   void SetPropertyInterface(iPropertyInterface* prop) override final;

   ColregSimulation::iSimulator* Get(navigation_dispatcher::iComServicePtr service) override final;

private:
   ColregSimulation::SIMULATION_PLAYER_TYPE getTypeFromExt(const char* ext) const;
   ColregSimulation::SIMULATION_PLAYER_TYPE correctXmlTypeByContent(const char* filename) const;

   iSimulatorPtr createSimulationPlayer(ColregSimulation::SIMULATION_PLAYER_TYPE type, navigation_dispatcher::iComServicePtr service);

private:
   iPropertyInterface* m_prop = nullptr;
   file_utils::global_path_storage m_paths;

   std::array<iSimulatorPtr, static_cast<size_t>(ColregSimulation::SIMULATION_PLAYER_TYPE::SPT_SIZE)> m_sims;
};